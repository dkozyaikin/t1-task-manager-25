package ru.t1.dkozyaikin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "view-profile";

    @NotNull
    public static final String DESCRIPTION = "View user profile";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final User user = getAuthService().getUser();
        System.out.println("ID: " + user.getId());
        System.out.println("login: " + user.getLogin());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
