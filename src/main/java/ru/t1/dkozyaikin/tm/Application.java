package ru.t1.dkozyaikin.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.component.Boostrap;

public class Application {

    public static void main(String[] args) {
        @NotNull Boostrap boostrap = new Boostrap();
        boostrap.run(args);
    }

}
